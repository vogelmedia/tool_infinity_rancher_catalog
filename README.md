# README

## Bitbucket\-Projekte holen
- https://bitbucket.org/vogelmedia/tool_infinity_broker
- https://bitbucket.org/vogelmedia/web_infinity_beps
- https://bitbucket.org/vogelmedia/web_infinity_purple
- https://bitbucket.org/vogelmedia/web_infinity_yellow
- https://bitbucket.org/vogelmedia/web_infinity_green
- https://bitbucket.org/vogelmedia/tool_infinity_terraform (optional)

## Mit docker-machine (virtualbox)

    # docker-machine installieren https://docs.docker.com/machine/install-machine/
        ## macOS
        curl -L https://github.com/docker/machine/releases/download/v0.10.0/docker-machine-`uname -s`-`uname -m` >/usr/local/bin/docker-machine && \
        chmod +x /usr/local/bin/docker-machine

        ## Linux
        curl -L https://github.com/docker/machine/releases/download/v0.10.0/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine &&
          chmod +x /tmp/docker-machine &&
          sudo cp /tmp/docker-machine /usr/local/bin/docker-machine

    ## Machine anlegen
        sudo docker-machine create -d virtualbox --virtualbox-memory "2048" --engine-insecure-registry p2-docker01:5000 --virtualbox-share-folder <Pfad zu deinem Workspace>:workspace rancher
    
    ## Machine betreten
        sudo docker-machine ssh rancher

    ## HOSTS erweitern in Server
        sudo vi /etc/hosts
        192.168.72.133 p2-docker01
        192.168.72.134 p2-docker02

    ## Rancher in docker starten
	    docker run -d --restart=unless-stopped -p 8080:8080 rancher/server
	    docker logs -f <CONTAINER_ID>
	
	## Rancher Agent / Host hinzufügen
        - Befehl für Agent kopieren und ausführen
	
	## Rancher um eigenen Katalog erweitern
        In Rancher den Katalog hinterlegen
        Infinity https://bitbucket.org/vogelmedia/tool_infinity_rancher_catalog.git
	
	## Maschine verlassen
	    exit
	
	## HOSTS erweitern in Host
        sudo gedit /etc/hosts   
        192.168.99.100 dev-www.industry-of-things.de

## Ohne docker-machine
    	
    ## HOSTS erweitern
        sudo gedit /etc/hosts
    
        192.168.72.133 p2-docker01
        192.168.72.134 p2-docker02 docker.industry-of-things.de
        127.0.0.1 dev-www.industry-of-things.de

    ## docker installieren
	    sudo apt install docker.io

    ## docker anpassen
        sudo gedit /etc/docker/daemon.json
    
        {
            "dns": ["192.168.6.229", "8.8.8.8", "9.9.9.9"],
            "insecure-registries":["192.168.72.133:5000", "p2-docker01:5000"]
        }

    ## Rancher in docker starten
        sudo docker run -d --restart=unless-stopped -p 8080:8080 rancher/server
        sudo docker logs -f <CONTAINER_ID>

    ## Rancher Agent / Host hinzufügen
        - Eigene IP anstelle von localhost hinterlegen
        - Befehl für Agent kopieren und ausführen

    ## Rancher um eigenen Katalog erweitern
        In Rancher den Katalog hinterlegen
        Infinity https://bitbucket.org/vogelmedia/tool_infinity_rancher_catalog.git

## Terraform installieren (aktuell optional)
- https://bitbucket.org/vogelmedia/tool_infinity_terraform

## Terraform starten (aktuell optional)
	terraform apply

## Ranger Host hinzufügen  (aktuell optional)
Befehl siehe terraform log